#!/bin/bash
set -eo

IMAGE_NAME=${REGISTRY_URL}/api-formular-adeverinte:$BITBUCKET_BRANCH-$BITBUCKET_COMMIT

echo ${DOCKER_PASSWORD} | docker login --username "$DOCKER_USERNAME" --password-stdin registry.digitalocean.com

if [ $BITBUCKET_BRANCH == "master" ]; then
  echo "Writing files for Production"
	echo $ENV_DACIA_PROD |base64 -d > .env
else
  echo "Writing files for Staging"
	echo $ENV_DACIA_STAGE |base64 -d > .env
fi

docker build -t ${IMAGE_NAME} .
