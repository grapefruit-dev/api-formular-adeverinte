#!/bin/bash
#!/bin/bash
set -eo

TAG=${BITBUCKET_BRANCH}-${BITBUCKET_COMMIT}

##################################################
echo "============Installing Dependencies========="
apt update -y
apt install -y jq wget

wget https://github.com/mikefarah/yq/releases/download/v4.24.2/yq_linux_amd64 -O /usr/bin/yq
chmod +x /usr/bin/yq

#############################################################3
git clone https://$INFRA_APP_USER:$INFRA_APP_TOKEN@bitbucket.org/grapefruit-dev/rcr-infrastructure.git
cd rcr-infrastructure
git remote set-url origin https://$INFRA_APP_USER:$INFRA_APP_TOKEN@bitbucket.org/grapefruit-dev/rcr-infrastructure.git
if [ $BITBUCKET_BRANCH == "master" ]; then
	echo "Deploying for Production"
	cd argocd/values/prod/api-formular-adeverinte/ &&  yq -i '.rcr.image.tag = "'$TAG'" | .rcr.initContainer.image.tag = "'$TAG'" | .rcr.cron.image.tag = "'$TAG'"' values.yaml
else
	echo "Deploying for Staging"
	cd argocd/values/stage/api-formular-adeverinte/ &&  yq -i '.rcr.image.tag = "'$TAG'" | .rcr.initContainer.image.tag = "'$TAG'" | .rcr.cron.image.tag = "'$TAG'"' values.yaml
fi

git config --global user.email "bcr@cloudhero.io"
git config --global user.name "CloudHero"
git add .
git commit --allow-empty -m "updated image tag to $TAG "
git push origin main
