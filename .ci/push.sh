#!/bin/bash
set -eo

IMAGE_NAME=${REGISTRY_URL}/api-formular-adeverinte:$BITBUCKET_BRANCH-$BITBUCKET_COMMIT

echo ${DOCKER_PASSWORD} | docker login --username "$DOCKER_USERNAME" --password-stdin registry.digitalocean.com
##################################################
# Push docker image
##################################################
docker push "${IMAGE_NAME}"
