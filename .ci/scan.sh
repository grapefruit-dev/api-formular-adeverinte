#!/bin/bash
set -eo

echo "
# HSTS 1.0 Known Hosts database for GNU Wget.
# Edit at your own risk.
# <hostname>    <port>  <incl. subdomains>      <created>       <max-age>
raw.githubusercontent.com       0       0       1695135648      31536000
gitlab.com      0       0       1698657820      31536000
pear.php.net    0       0       1699194225      31536000
mirrors.cqu.edu.cn      0       1       1684567176      31536000
iso.pop-os.org  0       1       1684566417      15552000
" > /root/.wget-hsts
chmod 644 /root/.wget-hsts

#export TRIVY_VERSION=$(wget -qO - "https://api.github.com/repos/aquasecurity/trivy/releases/latest" | grep '"tag_name":' | sed -E 's/.*"v([^"]+)".*/\1/')
#echo $TRIVY_VERSION
# Hard Coding Trivy Version for now
export TRIVY_VERSION="0.49.1"
wget --no-verbose https://github.com/aquasecurity/trivy/releases/download/v${TRIVY_VERSION}/trivy_${TRIVY_VERSION}_Linux-64bit.tar.gz -O - | tar -zxvf -

echo "Scanning filesystem"
./trivy filesystem .
#./trivy filesystem --scanners config,vuln --exit-code 0 --format template --template "@/contrib/sarif.tpl" -o 'trivy-results.sarif' .
