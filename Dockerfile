FROM registry.digitalocean.com/rcr-registry/php7.4:c07c21f734b3cddefacdabaab4f092ab125fbaea

VOLUME /var/www/api
WORKDIR /usr/src/app/
COPY . .


RUN composer update && \
    composer install -n

WORKDIR /var/www/api
