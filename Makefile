build:
	docker build -t registry.digitalocean.com/rcr-registry/api-formular-adeverinte:latest -f Dockerfile .

push:
	docker push registry.digitalocean.com/rcr-registry/api-formular-adeverinte:latest

all: build push
