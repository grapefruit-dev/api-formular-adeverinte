<?php

class Config {
	var $style;
	var $module;
	var $general = array ();
	var $_VARS   = array ();
	var $cookie	 = array ();
	var $rights  = array ();
	var $active = array ();
	var $dealers = array ();
	var $contacttype = array ();
	var $locid = array ();

	var $modules = array ();
    var $methods = array ();
	var $adminmodules = array ();
	var $language = array ();
	var $styles = array ();

	/**
	 * Config::Config()
	 *
	 *  constructor
	 *
	 * @return
	 */
	function Config () {
		$this->style = "";
		$this->module = "";
	    $this->general = array ();
		$this->general['appname'] = $_SERVER['HTTP_HOST'];
		$this->general['siteurl'] = "https://".$_SERVER['HTTP_HOST']."/";
		$this->general['sendmail'] = TRUE;
		$this->general['adminmail'] = "webmaster@grapefruit.ro";
		$this->general['version'] = "0.11";

	    $this->_VARS   = array ();
		$this->cookie = array (
			"cookie_id"  	=>  "GRUPRENAULT_API_",
			"cookie_domain" =>  "",
			"cookie_path"   =>  "/",
			"cookieencode"  =>  "1980",
		);

        $this->methods = array(
                            1 => array(
                                   'getTimestamp',
                                   'deleteInactiveData',
                                   'deleteOldData',
                               ),
                            2 => array(
                                   'getCertificatesList',
                                   'getFile',
                                   'confirmCertificatesList',
                                   'deleteCertificatesList',
                                   'setCertificateStatus',
                               ),
                           3 => array(
                                   'addCertificate',
                               ),
                           );
        $this->portal = array('gruprenaultro');
        $this->allowedIPs = array('192.168.0.1', '192.168.0.134', '80.86.123.96', '86.174.124.22', '31.15.12.170', '89.149.61.129', '89.45.175.172', '80.86.107.18');
        $this->APIKeys = array(
                'NZvXGPaWD4LbRd5jVC8QwQEX49tVSrdTaDLYy7zy' => array('acl' => '1'),
                'msEGabuJe7jhN4AaWD4PAWD4Le7QfhAJVLqjQ8JV' => array('acl' => '2'),
                'qJeN4AQfhAJVEJVaWD4LaWD4G7jhmsbuLPAe7jQ8' => array('acl' => '3'),
        );

	}

}

?>
