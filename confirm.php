<?php
header('Access-Control-Allow-Origin: *');
error_reporting(0);
session_start();

$dir = dirname (__FILE__);
chdir ($dir);
$root			= $dir."/";

require $root.'config.php';
$config = new Config;

require $root.'functions.php';



$style = "style";

$root_smarty    = "smarty/libs/";

require ($root_smarty.'Smarty.class.php');
$smarty = new Smarty;
$smarty->compile_check 	= true;
$smarty->debugging 		= false;
$smarty->template_dir   = $root."/".$style."/tpl";
$smarty->compile_dir    = $root."/tpl_c";



require ($root.'db.php');
$DB = new DB;
$DB->connectDB ();

$useractivate = addslashes(trim(strip_tags($_GET['useractivate'])));

$certificate = getList("certificate_requests", "id", "WHERE `active` IN (0, 1) AND `useractivate`='".$useractivate."' LIMIT 1", "*", "", true);

if ($certificate['id'] > 0){

    $nr_inregistrare = "A" . $certificate['id'];

    if ($certificate['active'] == 0){
    	$_SESSION['nr_inregistrare'] = $nr_inregistrare;

    	$DB->query ("UPDATE `certificate_requests` SET active = 1, status = 1, register_id = '".$nr_inregistrare."' WHERE active = 0 AND  `useractivate` = '".$useractivate."' ");

    	$certificate['user_data_decrypt'] =  json_decode(decrypt($certificate['user_data']));
    	$certificate['register_id'] = $nr_inregistrare;

    	$email = decrypt($certificate['email']);
    	$smarty->assign ("certificate", $certificate);

        $msg = $smarty->fetch ("activated-email.tpl");

        if ($_SERVER['HTTP_HOST'] === "adeverinte-dacia.ro"){
            send_mail_sendgrid($email, 'info@adeverinte-dacia.ro', "Solicitare cerere inregistrata", $msg);
        } else {
            send_mail_sendgrid($email, 'info@adeverinte-dacia.gd.ro', "Solicitare cerere inregistrata - dev", $msg);
        }
    }

    $DB->close_db ();
    //TODO: http://dacia.ro/solicitare-adeverinte-pensionare/confirmare.html
    header("Location: https://www.dacia.ro/solicitare-adeverinte-pensionare/confirmare.html?nr=".$nr_inregistrare);
} else {
	$DB->close_db ();
	header("Location: http://dacia.ro/solicitare-adeverinte-pensionare/404");
}

exit();
?>
