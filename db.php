<?php

class DB {

	public static $obj;
	public static $adminmail;
	private static $query_id;
	private static $db;
	private static $query_count;
	private static $query_time;
	private static $error;
	private static $errno;
	private static $sqlstate;
	private static $reporterror;
	private static $record_row;
	public static $debug;
	public static $instance;

	public static function getInstance ($id="default") {
		if( self::$instance[$id] == null) {
        	self::$instance[$id] = new self;
      	}
      	return self::$instance[$id];
    }

    public function init () {

    }

	public function DB () {
		if ( preg_match ("/adeverinte-dacia-api.rcr.gd.ro/", $_SERVER['HTTP_HOST']) ) {
			$this->obj = array (
				"sql_db"         => "adeverinte_dacia",
				"sql_user"       => "apiadeverintedacia",
				"sql_pass"       => "S1s1fOL2S1f8OJ182sf1",
				"sql_host"       => "prod-mysql-do-user-2521336-0.c.db.ondigitalocean.com",
				"sql_port"       => 25060,
				"persistent"     => 0,
				"charset"        =>  "utf-8",
				"cached_queries" => array (),
			);
		} elseif ( preg_match ("/api-formular-adeverinte.gft/", $_SERVER['HTTP_HOST']) ) {
			$this->obj = array (
				"sql_db"         => "gruprenault-ro-api",
                "sql_user"       => "root",
                "sql_pass"       => "",
				"sql_host"       => "prod-mysql-do-user-2521336-0.c.db.ondigitalocean.com",
                "sql_port"       => 3306,
				"persistent"     => 0,
				"charset"        =>  "utf-8",
				"cached_queries" => array (),
			);
        } elseif ( preg_match ("/adeverinte-dacia.ro/", $_SERVER['HTTP_HOST']) ) {
			$this->obj = array (
				"sql_db"         => "adev_dacia_ro",
                "sql_user"       => "apiformular",
                "sql_pass"       => "1wgwn2312sSf18OJfOL",
                "sql_host"       => "prod-mysql-do-user-2521336-0.c.db.ondigitalocean.com",
                "sql_port"       => 25060,
				"persistent"     => 0,
				"charset"        =>  "utf-8",
				"cached_queries" => array (),
			);
        } elseif ( preg_match ("/adeverinte-dacia.rcr.gd.ro/", $_SERVER['HTTP_HOST']) ) {
			$this->obj = array (
				"sql_db"         => "adev_dacia_ro_stage",
                "sql_user"       => "apiformularstage",
                "sql_pass"       => "1wgwn2312sSf18OJfOLstage",
                "sql_host"       => "prod-mysql-do-user-2521336-0.c.db.ondigitalocean.com",
                "sql_port"       => 25060,
				"persistent"     => 0,
				"charset"        =>  "utf-8",
				"cached_queries" => array (),
			);
        }
		$this->adminmail    	= "webmaster@grapefruit.ro";
	    $this->query_id      	= "";
		$this->db 				= "";
	    $this->query_count   	= 0;
	    $this->query_time   	= 0;
	    $this->error			= "";
	    $this->errno   			= 0;
	    $this->reporterror 		= 1;
	    $this->record_row    	= array ();
	    $this->debug			= 0;
	    $this->instance         = array ();
	    $this->instance['default'] = self;
    }

    function connectDB () {
    	if ( $this->db = new mysqli($this->obj['sql_host'], $this->obj['sql_user'], $this->obj['sql_pass'], $this->obj['sql_db'], $this->obj['sql_port']) ) {
    		$this->alert ("connection established to database", 'online');
		} else {
            $this->alert ("no database connection", 'offline');
		}

		$this->db->set_charset ($this->obj['charset']);
    }

    //--------------------
    // Proceseaza un query
    //--------------------

    function query ($the_query, $debug=0) {
        $query_time_start = $this->getmicrotime ();
        $this->query_id = $this->db->query ($the_query);
        $query_time_end = $this->getmicrotime ();

        $this->query_time = $this->query_time + ($query_time_end - $query_time_start);

        if (! $this->query_id ) {
            $this->alert ("Query error : ".$the_query."", "query");
        }
        if ( $debug || $this->debug ) {
            echo $the_query."\r\n";
        }
        $this->query_count++;
        $this->obj['cached_queries'][] = $the_query;
        return $this->query_id;
    }

	function insertSanitized($id, $registerId, $hash, $user_data, $email_data, $file1, $file2, $file1ext, $file2ext, $downloaded, $status, $useractivate, $created, $updated, $active)
    {
        $stmt = mysqli_prepare(
            $this->db,
            "INSERT INTO `certificate_requests` (`id`, `register_id`, `hash`, `user_data`, `email`, `file_1`, `file_2`, `file_1_ext`, `file_2_ext`, `downloaded`, `status`, `useractivate`, `created`, `updated`, `active`)
					VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
        $stmt->bind_param(
            'issssssssiisssi',
            $id,
            $registerId,
            $hash,
            $user_data,
            $email_data,
            $file1,
            $file2,
            $file1ext,
            $file2ext,
            $downloaded,
            $status,
            $useractivate,
            $created,
            $updated,
            $active
        );

        $stmt->execute();
    }

	function existsRegisteredFormByCNP($cnp)
	{
		$query = "SELECT user_data FROM certificate_requests";

		$allUserData = $this->db->query($query);
        $allUsers = [];

		while($row = $allUserData->fetch_assoc())
		{
			$allUsers[] = $row['user_data'];
		}


		$allCNPs = array_map(function ($user){
			return json_decode(decrypt($user))->cnp;
		}, $allUsers);


		return in_array($cnp, $allCNPs);


	}


    //----------------------------
    // Selecteaza descriere eroare
    //----------------------------

    function geterror () {
    	$this->error = $this->db->error;
    	return $this->error;
    }

    function getsqlstate () {
    	$this->sqlstate = $this->db->sqlstate;
    	return $this->sqlstate;
    }

    function getprettyerror () {
    	$error = $this->error;
		if ( preg_match ("duplicate", $this->error) ) {
			$error = "Duplicate value\r\n";
			$error .= $this->error;
		}
		return $error;
    }

    //---------------------------
    // Selecteaza eroarea numarul
    //---------------------------

    function geterrno () {
        $this->errno = $this->db->errno;
       	return $this->errno;
    }


    //-------------------------------------------------
    // Selecteaza un row bazat pe ultimul query (Array)
    //-------------------------------------------------

    function fetch_array ($query_id = "") {
    	if ($query_id == "") {
    		$query_id = $this->query_id;
    	}
    	if ( $query_id ) {
        	$this->record_row = $query_id->fetch_array (MYSQLI_ASSOC);
        	return $this->record_row;
       	} else {
            exit ();
       	}
    }

    //-----------------------------------------
    // Selecteaza un row bazat pe ultimul query
    //-----------------------------------------

    function fetch_row ($query_id = "") {
    	if ($query_id == "") {
    		$query_id = $this->query_id;
    	}
        $this->record_row = $query_id->fetch_array (MYSQLI_ASSOC);
        return $this->record_row;
    }

	//-------------------------------------------------------
    // Selecteaza numarul de randuri afectat de ultimul query
    //-------------------------------------------------------

    function get_affected_rows () {
        return $this->db->affected_rows;
    }

    //--------------------------------------
    // Selecteaza numarul de randuri afectat
    //--------------------------------------

    function get_num_rows () {
        return $this->query_id->num_rows;
    }

    //-------------------------------------------------------
    // Selecteaza ultima insert id dintr-un sql autoincrement
    //-------------------------------------------------------

    function get_insert_id () {
        return $this->db->insert_id;
    }

    //------------------------------------
    // Returneaza numarul de query folosit
    //------------------------------------

    function get_query_cnt () {
        return $this->query_count;
    }

    //---------------------------------------
    // Returneaza timpul folosit pentru query
    //---------------------------------------

    function get_query_time () {
        return $this->query_time;
    }

    function free_result ($query_id="") {
   		if ($query_id == "") {
    		$query_id = $this->query_id;
    	}
    	@mysql_free_result ($query_id);
    }

    function close_db () {
        return $this->db->close();
    }

	function getmicrotime (){
   		list($usec, $sec) = explode(" ",microtime());
   		return ((float)$usec + (float)$sec);
	}

	function debugThis () {
	    ob_start ();
	    print_r ( $GLOBALS );
	    $content = ob_get_contents ();
	    ob_end_clean ();
	    return $content;
	}

    //--------------------------------
    // alert mail catre administrator
    //--------------------------------

    function alert ($msg, $action='online') {
    	global $root;
        require ($root."db/mysql.php");
        if ( is_array ($mysql) == FALSE ) {
            if ( $action == 'offline' ) {
                $arr['offline'] = 1;
            } elseif ( $action == 'online' ) {
                $arr['online'] = 0;
            }
        	$this->createArray ($arr);
        	require ($root."db/mysql.php");
        }

        if ( $action == 'offline' ) {
            if ( $mysql['offline'] == 0 ) {
                $arr = array ();
                $arr['offline'] = 1;
                $this->createArray ($arr);
            } elseif ( $mysql['offline'] == 1 ) {
                include ($root."db/error-message.html");
                exit ();
            }
        } elseif ( $action == 'online' ) {
            if ( $mysql['offline'] == 1 ) {
                $arr = array ();
                $arr['offline'] = 0;
                $this->createArray ($arr);
            } elseif ( $mysql['offline'] == 0 ) {
				return FALSE;
            }
		}

      	$this->geterror ();
      	$this->geterrno ();
      	$this->getsqlstate ();

        $critical = 0;
        if ( substr($this->getsqlstate (), -2, 2) == "02"  ) {
            $critical = 1;
       	}

      	if ( $this->reporterror == 1 && $critical == 1 ) {
        	$msg ="Database error in ".$_SERVER['HTTP_HOST']."\r\n\r\n$msg\r\n\r\n";
        	$msg .="mysql error: " . $this->error . "\r\n";
        	$msg .="mysql error number: " . $this->errno . "\r\n";
        	$msg .="mysql sql state: " . $this->sqlstate . "\r\n\r\n";
        	$msg .="Date: ".date("l dS of F Y h:i:s A")."\r\n";
        	$msg .="Script: " . $_SERVER['REQUEST_URI'] . "\r\n";
        	$msg .="Referer: ".$_SERVER['HTTP_REFERER']."\r\n";
        	$msg .="IP: ".$_SERVER['REMOTE_ADDR']."\r\n";
        	$msg .="Host: ". gethostbyaddr( $_SERVER['REMOTE_ADDR'] ) ."\r\n";
        	$msg .="\r\n";
        	$msg .="\$GLOBALS\r\n";
        	$msg .=$this->debugThis ();

          	$this->sendMailToAdmin ($this->adminmail, $this->adminmail, $this->appname." Database error in ".$_SERVER['HTTP_HOST']."!", $msg);

	        //include ($root."db/error-message.html");
    	}
    	// critical error
    	if ( $critical == 1 ) {
    		exit ();
    	}
  	}

  	function sendMailtoAdmin ($to, $from, $subject, $message) {
  		$headers = "From: \"".$from."\" <".$from.">\n";
		$headers .= "Reply-To: ".$from."\n";

		@mail ($to, $subject, $message, $headers);
  	}

  	function createArray ( $arr, $file="./db/mysql.php" ) {
  		global $root;
  		$file= $root."db/mysql.php";
  	    $content = "<?php\r\n";
  	    $content .= "\$mysql = array ();\r\n";
  	    foreach ( $arr as $k => $v ) {
  	        $content .= "\$mysql['".$k."']\t=\t'".$v."';\r\n";
  	    }
  	    $content .= "?>";
  	    $this->writetoFile ($file, $content);
  	}

	function writetoFile ($path, $data, $backup=0) {
		if ( file_exists ($path) ) {
    		if ($backup==1) {
    			$filenamenew = $path."venom";
      			rename ($path, $filenamenew);
    		}
		}

		if ($data!="") {
    		$filenum=fopen($path,"w");
    		fwrite($filenum, $data);
			fclose($filenum);
		}
	}

} // end class

?>
