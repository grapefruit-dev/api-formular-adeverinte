<?php


function parseUrl($url){
    $arr = array();

    // parse GET params
    $query_str = parse_url($url, PHP_URL_QUERY);
    parse_str($query_str, $arr['get_params']);

    // parse URL params
    $url = explode('?', $url);
    $url_arr = explode('/', $url[0]);

    $arr['method'] = $url_arr['3'];
    unset($url_arr['3']);

    foreach ($url_arr as $k => $v){
        if ($k > 4){
            $params = explode(':', $v);
            $arr['url_params'][trim(addslashes(strip_tags($params['0'])))] = trim(addslashes(strip_tags($params['1'])));
        }
    }
    return $arr;
}

function getPostParams(){
    $arr = array();

    if (count($_POST) == 0){
        $post_params = json_decode( file_get_contents('php://input'), true);
    } else {
        $post_params = $_POST;
    }


    $arr = $post_params;

    return $arr;
}

function validCNP($p_cnp) {
    // CNP must have 13 characters
    if(strlen($p_cnp) != 13) {
        return false;
    }
    $cnp = str_split($p_cnp);
    unset($p_cnp);
    $hashTable = array( 2 , 7 , 9 , 1 , 4 , 6 , 3 , 5 , 8 , 2 , 7 , 9 );
    $hashResult = 0;
    // All characters must be numeric
    for($i=0 ; $i<13 ; $i++) {
        if(!is_numeric($cnp[$i])) {
            return false;
        }
        $cnp[$i] = (int)$cnp[$i];
        if($i < 12) {
            $hashResult += (int)$cnp[$i] * (int)$hashTable[$i];
        }
    }
    unset($hashTable, $i);
    $hashResult = $hashResult % 11;
    if($hashResult == 10) {
        $hashResult = 1;
    }
    // Check Year
    $year = ($cnp[1] * 10) + $cnp[2];
    switch( $cnp[0] ) {
        case 1  : case 2 : { $year += 1900; } break; // cetateni romani nascuti intre 1 ian 1900 si 31 dec 1999
        case 3  : case 4 : { $year += 1800; } break; // cetateni romani nascuti intre 1 ian 1800 si 31 dec 1899
        case 5  : case 6 : { $year += 2000; } break; // cetateni romani nascuti intre 1 ian 2000 si 31 dec 2099
        case 7  : case 8 : case 9 : {                // rezidenti si Cetateni Straini
            $year += 2000;
            if($year > (int)date('Y')-14) {
                $year -= 100;
            }
        } break;
        default : {
            return false;
        } break;
    }
    return ($year > 1800 && $year < 2099 && $cnp[12] == $hashResult);
}


function addLog ($params) {
    global $DB;

    $actv = 1;
    if ($params['active']){
        $actv = $params['active'];
    }
	$DB->connectDB ();
    $DB->query ("INSERT INTO `log` (`id`, `method`, `api_key`, `request`, `ip`, `date`, `active`)
    VALUES (NULL
    , '".$params['method']."'
    , '".$params['api_key']."'
    , '".$params['request']."'
    , '".getIP()."'
    , '".date("Y-m-d H:i:s")."', '".$actv."');");
	$DB->close_db ();
}


function getList ($table, $key="id", $query, $select, $return="", $returnrow="", $show_sql=0) {
    global $DB;
    $ret = array ();
    $q = $DB->query ("SELECT ".$select." FROM ".$table." ".$query."",$show_sql);
    while ( $row = $DB->fetch_array ($q) ) {
        if ( $return ) {
            return $row[$return];
        } elseif ( $returnrow ) {
            return $row;
        }
        if ( $key ) {
            $ret[$row[$key]] = $row;
        } else {
            $ret[] = $row;
        }
    }
    return $ret;
}


//-----------------
// Start Set Cookie
//-----------------
function set_Cookie ($name, $value = "", $sticky = 0) {
    global $config;

    $exipres = "";

    if ($sticky == 1) {
        $expires = time() + 60*60*24*365;
    }

    $name = $config->cookie['cookie_id'].$name;
    $newValue = encodeC ($value);

    @setcookie($name, urlencode($newValue), $expires, $config->cookie['cookie_path'], $config->cookie['cookie_domain']);
}
// End Set Cookie


//-----------------
// Start Get Cookie
//-----------------
function get_Cookie ($name) {
    global $config;

    if ( isset( $_COOKIE[$config->cookie['cookie_id'].$name] ) ) {
        $cookie = urldecode ( $_COOKIE[$config->cookie['cookie_id'].$name] );
        return decodeC ($cookie);
    } else {
        return FALSE;
    }
}
// End Get Cookie


//-------------
// Start Encode
//-------------
function encodeC ($cookie) {
    global $config;

    $newcookie = array ();
    $cookie = base64_encode ($cookie);

    for ( $i=0; $i<=strlen ($cookie); $i++ ) {
        $newcookie[] = ord ( $cookie[ $i ] ) * $config->cookie['cookieencode'];
    }

    $newcookie = implode ('.', $newcookie);
    return $newcookie;
}
// End Encode


//--------------
// Start Dencode
//--------------
function decodeC ($oldcookie) {
    global $config;

    $cookie = explode ('.', $oldcookie);
    $newcookie = array ();

    for ( $i=0; $i<=strlen ($oldcookie); $i++ ) {
        $newcookie[] = chr ( $cookie[ $i ] / $config->cookie['cookieencode'] );
    }

    $newcookie = implode ('', $newcookie);
    $newcookie = base64_decode ($newcookie);

    return $newcookie;
}
// End Decode


function translateDate ($d, $lang="ro") {
	global $config;
		if ( substr ($d, 0, 1) == "0" ) {
			$d = substr ($d, 1);
		}

		  $d = str_replace ("January", "Ianuarie", $d);
		  $d = str_replace ("February", "Februarie", $d);
		  $d = str_replace ("March", "Martie", $d);
		  $d = str_replace ("April", "Aprilie", $d);
		  $d = str_replace ("May", "Mai", $d);
		  $d = str_replace ("June", "Iunie", $d);
		  $d = str_replace ("July", "Iulie", $d);
		  $d = str_replace ("August", "August", $d);
		  $d = str_replace ("September", "Septembrie", $d);
		  $d = str_replace ("October", "Octombrie", $d);
		  $d = str_replace ("November", "Noiembrie", $d);
		  $d = str_replace ("December", "Decembrie", $d);

		  $d = str_replace ("Monday", "Luni", $d);
		  $d = str_replace ("Tuesday", "Marti", $d);
		  $d = str_replace ("Wednesday", "Miercuri", $d);
		  $d = str_replace ("Thursday", "Joi", $d);
		  $d = str_replace ("Friday", "Vineri", $d);
		  $d = str_replace ("Saturday", "Sambata", $d);
		  $d = str_replace ("Sunday", "Duminica", $d);

	return $d;
}



function pages ($total, $link, $rows, $page, $name, $pagename='page', $addpage=0) {
	global $smarty;

	if ($total <= $rows) {
		//return "";
	}

	// navigation pages
	$pages = 6;

	$min = ($page - 1) * $rows + 1;
	$max = $page * $rows;
	if ($max > $total) {$max = $total;}

	$nrpages = ceil ($total/$rows);

	if ($total <= $rows) {
		$smarty->assign ("curpag", $page);
		$smarty->assign ("totpag", $nrpages);
		$smarty->assign ("totrez", $total);
		$pages = $smarty->fetch ("pages.tpl");
		return $pages;
	}

	//$infopages = "".$min." la ".$max." din ".$total."<br>";
	//$infopages .= $nrpages." pagini | \r\n";

	for ($i=1 ; $i<=$nrpages ; $i++) {

		if ( $i <= $page - $pages or $i >= $page + $pages ) {
			if ($i==1) {
				$firstpageret = "<a href=\"".$link."&".$pagename."=".$i."\" class=\"pagelink\" rel=\"".$i."\" title=\"Prima pagina\">&laquo;</a> ... \r\n";
			}
			   if ($i==$nrpages) {
				$lastpageret = " ... <a href=\"".$link."&".$pagename."=".$i."\" class=\"pagelink\" rel=\"".$i."\" title=\"Ultima pagina\">&raquo;</a>\r\n";
			}
		}

		if ($page > 1) {
			$prevpage = $page - 1;
			if ( $prevpage == 1 && $addpage == 0 ) {
				$prevpageret = "".$link."&".$pagename."";
			} else {
				$prevpageret = "".$link."&".$pagename."=".$prevpage."";
			}
		}

		if ($i <= $page + $pages and $i >= $page - $pages) {
			if ($page == $i) {
				$ret .= "<span>".$i."</span> ";
			} else {
				if ( $i == 1 && $addpage == 0 ) {
					$ret .= "<a href=\"".$link."&".$pagename."\" class=\"pagelink\" rel=\"".$i."\" title=\"Pagina ".$i."\">".$i."</a>";
				} else {
					$ret .= "<a href=\"".$link."&".$pagename."=".$i."\" class=\"pagelink\" rel=\"".$i."\" title=\"Pagina ".$i."\">".$i."</a>";
				}
			}
		}

		if ($page < $nrpages) {
			$nextpage = $page + 1;
			$nextpageret = "".$link."&".$pagename."=".$nextpage."";
		}

	}

	$smarty->assign ("backurl", $prevpageret);
	$smarty->assign ("nexturl", $nextpageret);
	$smarty->assign ("backpage", $prevpage);
	$smarty->assign ("nextpage", $nextpage);
	$smarty->assign ("curpag", $page);
	$smarty->assign ("totpag", $nrpages);
	$smarty->assign ("totrez", $total);
	$smarty->assign ("pagesurl", $ret);
	$pages = $smarty->fetch ("pages.tpl");
	return $pages;
	//return $infopages.$firstpageret.$prevpageret.$ret.$nextpageret.$lastpageret;

}


function pagesCheck ($page, $total, $offset=10) {
	$offset = $offset ? $offset : 1;
	$ret = array ();
	$ret['offset'] 	= $offset ? $offset : 1;
	$ret['page'] 	= $page;
	$ret['page'] 	= (int) $ret['page'] ? $ret['page'] : 1;
	$ret['page'] 	= abs ( (int) $ret['page'] );
	$ret['min'] 	= ($ret['page'] - 1) * $offset;
	$pages = ceil ($total / $offset);
	if ( $ret['page'] > $pages ) {
		$ret['page'] 	= 1;
		$ret['min']  	= 0;
	}
	return $ret;
}

function send_mail_sendgrid ($to, $from, $subject, $msg) {
    require 'vendor/autoload.php';

    $message = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">\n";
    $message .= "<HTML>\n";
    $message .= "<HEAD>\n";
    $message .= "<META http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n";
    $message .= "<title>".$subject."</title>\n";
    $message .= "</HEAD>\n";
    $message .= "<BODY>\n";
    $message .= $msg;
    $message .= "</BODY>\n</HTML>";

    $mail = new \SendGrid\Mail\Mail();
    $mail->setFrom($from, 'Adeverinte Dacia');
    $mail->addTo($to);

    $mail->setSubject($subject);
    $mail->addContent("text/html", $message);

    $sendgrid = new \SendGrid("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");
    try {
        $sendgrid->send($mail);
    } catch (Exception $e) {
        echo "Message could not be sent. Sendgrid Error: {$e->getMessage()} \n";
    }
}

function send_mail_classSMTP ($to, $from, $subject, $msg, $replyto='') {
    $message = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">\n";
    $message .= "<HTML>\n";
    $message .= "<HEAD>\n";
    $message .= "<META http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n";
    $message .= "<title>".$subject."</title>\n";
    $message .= "</HEAD>\n";
    $message .= "<BODY>\n";
    $message .= $msg;
    $message .= "</BODY>\n</HTML>";

    include_once 'PHPMailer/src/Exception.php';
    include_once 'PHPMailer/src/PHPMailer.php';
    include_once 'PHPMailer/src/SMTP.php';

    $mail = new \PHPMailer\PHPMailer\PHPMailer(true);

    try {
        //Server settings
        $mail->CharSet = 'UTF-8';
        $mail->isSMTP();                                            // Set mailer to use SMTP
        $mail->Host       = 'smtp.mailtrap.io';                     // Specify main and backup SMTP servers
        $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
        $mail->Username   = 'cd0458350ffe4b';                       // SMTP username
        $mail->Password   = 'c5996acadc18cd';                       // SMTP password
        $mail->Port       = 2525;                                   // TCP port to connect to
        $mail->setFrom($from, 'Adeverinte Dacia');
        $mail->addAddress($to);                                     // Name is optional

        // Content
        $mail->isHTML(true);                                 // Set email format to HTML
        $mail->Subject = $subject;
        $mail->Body    = $message;

        $mail->send();
    } catch (Exception $e) {
        echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
    }

}

function send_mail ($to, $from, $subject, $msg, $replyto='') {
    if ($replyto == '') {
        $replyto = $from;
    }

    $headers = "MIME-Version: 1.0\n";
    $headers .= "Content-type: text/html; charset=utf-8\n";
    $headers .= "From: ".$from."\n";
    $headers .= "Reply-To: ".$replyto."\n";
    //$headers .= "X-Mailer: Mailer v0.1\n";

    $message = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">\n";
    $message .= "<HTML>\n";
    $message .= "<HEAD>\n";
    $message .= "<META http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n";
    $message .= "<title>".$subject."</title>\n";
    $message .= "</HEAD>\n";
    $message .= "<BODY>\n";
    $message .= $msg;
    $message .= "</BODY>\n</HTML>";

    global $root;


    include_once 'PHPMailer/src/Exception.php';
    include_once 'PHPMailer/src/PHPMailer.php';
    include_once 'PHPMailer/src/SMTP.php';

    $mail = new \PHPMailer\PHPMailer\PHPMailer(true);

    //Usual setup
    //$mail = new PHPMailer;
    $mail->CharSet = 'UTF-8';
    $mail->setFrom($from, 'Adeverinte Dacia');
    $mail->addAddress($to);
    $mail->addBCC('cosmin.panait@gmp.webstyler.ro');
    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = $subject;
    $mail->Body    = $message;
    //$mail->msgHTML(file_get_contents('contents.html'), __DIR__);

    //This should be the same as the domain of your From address
    $mail->DKIM_domain = 'adeverinte.daciagroup.com';
    //See the DKIM_gen_keys.phps script for making a key pair -
    //here we assume you've already done that.
    //Path to your private key:
    $mail->DKIM_private_string = '-----BEGIN RSA PRIVATE KEY-----
MIIEpAIBAAKCAQEAwsl2OL/V2T03Mwqfx25GqDAeyAxm8CR70yz/sfVz+9NSe5T7
UW+vfWgXZ0J1oLmRB/xWmduyIx/YicANngKgxaOpheE5l8NGfAn2k7bdShGgFbah
yJS9KTnJ3rRUxnoTOpvGS46cx+2pdLnsxUkVason8EZEDn6Djp2kDzd3khibyhTB
rc8CVT2uWz7xn+K8r1byKkrnrYxN9BTo/eLinVIjuAjmjCD8ZO3ZTmlXL1MuR0cZ
SPP+Zj5IyQRvHfU9kH3W/5vsjc57rBy6TJZ8mIdGtNjOH2PJnZgfq3cJoCh9vhSW
v9I2wDuY2Luq6NTVCUte94V9vod3uzN/MCDAWQIDAQABAoIBAQCYZRozaBMddYFc
U7GSTDrHl25dwSZAN6sRGyGpz4nAuiLMQVEm86RvDyNQTHTPHJ/ViZ0Pnw0N4fp+
O13sA85ZEqSs8/slO0hH/1uFUgprysJeepa04XnlaSsBLEEvT47/yaQotRkAanar
tlMCPx2cgTEiETdpx52R1M/of9Ly3yMbANrN3w8GOfpEIjNZqZNm/L90t+EGknfr
B+qaunlkcvy6HNrY1NnsEy+hfrSVjXzWZCI4fNXVY3jJFCMTGtJsvMdS80G8y4BO
GX1dncEGkBjjclYPZFrarQj8FzETfZ2hSbQlyOTPcVQZkMsbyeUoJyO+kvPEDHWG
RAJXwH3pAoGBAMwDpvRf4peG1KDe4Z2Yn313aQUXJRxDwH4ATxIGcbTrHoPlNyJB
YO+qICXNPPvzn271AcU5LYPszGNp5lsE4BVNdT6JaiDNUk4zft7KeAJ4wKUKbfIk
IZKRte1w/Si/myy4jVmXD2spXMbwYpI4mkITuAO3HyMh79pwtrqOrbazAoGBAPRr
49UUW34Aivsug8Z2sm5RTU5WPnXQkfQXroZ1rysV02XDDeUi1uIJahOfTEzAusFZ
WxBbOyDSpkJqESqZaF6OOpfN0zBvpU18pE+QpHhddgz7nStHOg468jRrbKlk2Ovu
5PJAAxlvZZFGzAwDiIvwbnryepzrvJpkO4T0MhLDAoGBAIVYAcCKB11IuCMpakeK
YPwtlwvHs+7EbcUtSIEciydX4csNfzq5hrU++eIbQjffZR6trumMVK58kJ6Nsg1d
o2TpCq/EOwSTjq8kQ1E0/rmG2UtGBNSWPwdAi6tVBXKjwbW6LlEXRfGhyE0TAypV
KqXzo3tyLjzVSqL4tsY6yVm3AoGAdN5Bidi6Bn1r9r4+8949YtItDI/gPaUEpJnO
MSyEHS3KbpXdRvpplJq7xbYdXXWp1llLWX7FvuP56zecDapcZRnMmlkn6bj4aRx3
1DNs8MB/eaW1eMQN1lAJD1duFXDK58TKY6qH0NmazuPzHKtz1rWXneHjDrUI8SmL
6/53EtECgYBzAOWmbUAWM/2atiYu1cXjLmub/hOlc4VGxkhmTLEoiGOhWuqWE3Za
J4C5LUXAf5LO684TPJSX/GgmMooGZVFGr7OfaZZ47D0m4O2iJmgp5x47fKfBYqlk
cGHk4+9vlP5VBgHuRXig4tDhq7ufpfg/LpatdZQVUmkBE//nS7YxhQ==
-----END RSA PRIVATE KEY-----
';
    //Set this to your own selector
    $mail->DKIM_selector = 'dkim';
    //Put your private key's passphrase in here if it has one
    $mail->DKIM_passphrase = '';
    //The identity you're signing as - usually your From address
    $mail->DKIM_identity = $mail->From;
    //Suppress listing signed header fields in signature, defaults to true for debugging purpose
    $mail->DKIM_copyHeaderFields = false;
    //Optionally you can add extra headers for signing to meet special requirements
    //$mail->DKIM_extraHeaders = ['List-Unsubscribe', 'List-Help'];


    if (!$mail->send()) {
        echo 'Mailer Error: '. $mail->ErrorInfo;
    } else {
        //echo 'Message sent!'.$mail->getSentMIMEMessage();;
    }

}

function send_mail_old ($to, $from, $subject, $msg, $replyto='') {
  	if ($replyto == '') {
		$replyto = $from;
	}

	$headers = "MIME-Version: 1.0\n";
	$headers .= "Content-type: text/html; charset=utf-8\n";
  	$headers .= "From: ".$from."\n";
	$headers .= "Reply-To: ".$replyto."\n";
	//$headers .= "X-Mailer: Mailer v0.1\n";

	$message = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">\n";
	$message .= "<HTML>\n";
    $message .= "<HEAD>\n";
    $message .= "<META http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n";
    $message .= "<title>".$subject."</title>\n";
    $message .= "</HEAD>\n";
    $message .= "<BODY>\n";
	$message .= $msg;
	$message .= "</BODY>\n</HTML>";

    mail ($to, $subject, $message, $headers);

}

function isJson($string) {
    json_decode($string);
    return json_last_error() === JSON_ERROR_NONE && !is_numeric($string);
}

function isEmail ($email) {
    return (!preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $email)) ? false : true;
}

function isMobilePhone ($tel) {
    return preg_match("/^07[0-9]{8}$/", $tel) || preg_match("/^03[0-9]{8}$/", $tel) ||
        preg_match("/^02[0-9]{8}$/", $tel);
}

function getIP () {
	if ( $_SERVER['HTTP_X_FORWARDED_FOR'] ) {
        if ( preg_match (",", $_SERVER['HTTP_X_FORWARDED_FOR']) ) {
    		$ip = explode (",", $_SERVER['HTTP_X_FORWARDED_FOR']);
    		return addslashes ( trim ($ip[0]) );
        } else {
            return addslashes ($_SERVER['HTTP_X_FORWARDED_FOR']);
        }
	} else {
		return $_SERVER['REMOTE_ADDR'];
	}
}

function getFileext ($file) {
	return strtolower ( substr(strrchr($file,"."),1) );
}

function socketOpen ($host, $port, $method='GET', $path, $string, $header='', $debug=0) {

    if ($fp = @fsockopen($host, $port, $errno, $errstr, $timeout = 15)) {

		$h = $method." ".$path." HTTP/1.0\r\n";
		$h .= "Host: ".$host."\r\n";
		if ( $method == "GET" ) {
			$h .= "Content-Type: text/html; charset=utf-8\r\n";
		} else {
			$h .= "Content-Type: application/x-www-form-urlencoded\r\n";
		}
		$h .= "Content-length: ".strlen($string)."\r\n";
		//$h .= "Pragma: no-cache\r\n";
		//$h .= "Cookie: SENAT_screen_width=1280; dragable_rss_boxesundefined=none; ASP.NET_SessionId=ja11q4rg0rm3j345lkypuj55\r\n";
		//$h .= "User-Agent: Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Trident/4.0; GTB6.4; .NET CLR 2.0.50727; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729)\r\n";

        if ( preg_match ("senat", $host) ) {
            //$h .= "Accept: image/gif, image/jpeg, image/pjpeg, image/pjpeg, application/x-shockwave-flash, application/x-ms-application, application/x-ms-xbap, application/vnd.ms-xpsdocument, application/xaml+xml, application/x-silverlight, */*\r\n";
            //$h .= "Referer: http://www.senat.ro/Voturiplen.aspx\r\n";
            //$h .= "Accept-Language: en-us\r\n";
            //$h .= "Connection: Keep-Alive\r\n";
            //$h .= "Pragma: no-cache\r\n";
        }

        if ( $header ) {
            $h .= $header."\r\n";
        }

        $h .= "\r\n";
        //$h .= "Connection: close\r\n\r\n";
        $h .= $string;
        $h .= "\r\n\r\n";

		if ( $debug == 1 ) {
			echo $h;
		}

        fputs ($fp, $h);

        $res = "";
        while( !feof($fp) ) {
            $res .= fgets($fp, 4096);
        }
        @fclose($fp);

		if ( preg_match ("<\/html>", $res) == false ) {
			//echo "++++++++++++ error reading ".$host." ".$method." ".$path.", retring\r\n";
			//sleep (2);
			//return socketOpen ($host, $port, $method, $path, $string, $header);
		}

		return $res;
    } else {
        return false;
    }
}

define('SALT', 'qJeN4AQfhAJVEJVaWD4LaWD4G7jhmsbu');

function encrypt($plaintext) {
    $method = "AES-256-CBC";
    $key = hash('sha256', SALT, true);
    $iv = openssl_random_pseudo_bytes(16);

    $ciphertext = openssl_encrypt($plaintext, $method, $key, OPENSSL_RAW_DATA, $iv);
    $hash = hash_hmac('sha256', $ciphertext . $iv, $key, true);

    return base64_encode($iv . $hash . $ciphertext);
}

function decrypt($ivHashCiphertext) {
    $method = "AES-256-CBC";
    $iv = substr(base64_decode($ivHashCiphertext), 0, 16);
    $hash = substr(base64_decode($ivHashCiphertext), 16, 32);
    $ciphertext = substr(base64_decode($ivHashCiphertext), 48);
    $key = hash('sha256', SALT, true);

    if (!hash_equals(hash_hmac('sha256', $ciphertext . $iv, $key, true), $hash)) return null;

    return openssl_decrypt($ciphertext, $method, $key, OPENSSL_RAW_DATA, $iv);
}

// function encrypt($text) {
    // return trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, SALT, $text, MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND))));
// }
//
// function decrypt($text) {
    // return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, SALT, base64_decode($text), MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND)));
// }

//define('SALTT', '0A0B0C0D0E0F101112131415160D0E0F101112131415161718191A1B1C1D1E1F');
//define('SALTT', 'qJeN4AQfhAJVEJVaWD4LaWD4G7jhmsbu');
define('SALTT', 'RdWDLa9tVC8Q4LbVSrdd5jJVwQEX4D4a');

function encrypySimpleAES($plaintext){
    $ciphertext_raw = openssl_encrypt($plaintext, "AES-256-CBC", SALTT, OPENSSL_RAW_DATA );
    $ciphertext = base64_encode($ciphertext_raw);
    return $ciphertext;
}


function decrypySimpleAES($ciphertext){
    $c = base64_decode($ciphertext);
    $original_plaintext = openssl_decrypt($c, "AES-256-CBC", SALTT, OPENSSL_RAW_DATA );
    return $original_plaintext."\n";
}


?>