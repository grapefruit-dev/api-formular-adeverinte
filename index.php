<?php
header('Access-Control-Allow-Origin: *');
error_reporting(0);
session_start ();

$dir = dirname (__FILE__);
chdir ($dir);
$root			= $dir."/";

require $root.'config.php';
$config = new Config;

require $root.'functions.php';

$style = "style";

$root_smarty    = "smarty/libs/";

require ($root_smarty.'Smarty.class.php');
$smarty = new Smarty;
$smarty->compile_check 	= true;
$smarty->debugging 		= false;
$smarty->template_dir   = $root."/".$style."/tpl";
$smarty->compile_dir    = $root."/tpl_c";

require ($root.'db.php');
$DB = new DB;

/*
Error codes
code    message
1   userIdExists
2   Missing required parameter
6   hashExists
7   loginExists
9   Bad portal, should be one of these
10  No method specified
11  tooShortLogin
12  badPassword
13  Unable to send registration email for user
100 Unknown error
101 Forbidden - No APIKey
101 Forbidden - Bad APIKey
101 Forbidden - No access right
101 Forbidden - IP restriction
*/

//TODO: verify IP
/*if (!in_array(getIP(), $config->allowedIPs) && !$result['error']){
    $result = array('error' => true, 'errorCodeNumber' => 101, 'errorCode' => 'Forbidden - IP restriction');
}*/

// set acl 1 - basic access
$acl = 1;

// get URL & URL params
if(!$result['error']){
    $getParams = parseUrl($_SERVER['REQUEST_URI']);
}

// verify APIKey
if ($getParams['get_params']['APIKey'] == '' && !$result['error']){
    $result = array('error' => true, 'errorCodeNumber' => 101, 'errorCode' => 'Forbidden - No APIKey');
} else if ($getParams['get_params']['APIKey'] != '' && !array_key_exists($getParams['get_params']['APIKey'], $config->APIKeys) && !$result['error']){
    $result = array('error' => true, 'errorCodeNumber' => 101, 'errorCode' => 'Forbidden - Bad APIKey');
}

// set acl 3 - addCertificate
if($config->APIKeys[$getParams['get_params']['APIKey']]['acl'] == 3 && $getParams['method'] == 'addCertificate' && !$result['error']){
    require ($root.'methods.php');

    $result = addCertificate();

    $json_result = json_encode($result, JSON_PRETTY_PRINT);

    header('Content-type: application/json');
    echo $json_result;
    exit();
}

// set acl 2 - full access
if($config->APIKeys[$getParams['get_params']['APIKey']]['acl'] == 2){
    $acl = 2;
}

// verify method exists & valid
if ($getParams['method'] == '' && !$result['error']){
    $result = array('error' => true, 'errorCodeNumber' => 10, 'errorCode' => 'No method specified');
} else if(!in_array($getParams['method'], $config->methods['1']) && !in_array($getParams['method'], $config->methods['2']) && !$result['error']){
    $result = array('error' => true, 'errorCodeNumber' => 10, 'errorCode' => 'Invalid method');
}

// verify method access
if(array_key_exists($getParams['get_params']['APIKey'], $config->APIKeys)
        && !in_array($getParams['method'], $config->methods[$acl])
        && (in_array($getParams['method'], $config->methods[2]) || in_array($getParams['method'], $config->methods[3]))
        && !$result['error']
    ){
    $result = array('error' => true, 'errorCodeNumber' => 101, 'errorCode' => 'Forbidden - No access right');
}

// verify portal
//if (!in_array($getParams['url_params']['portal'], $config->portal) && !$result['error']){
    //$result = array('error' => true, 'errorCodeNumber' => 9, 'errorCode' => 'Bad portal, should be one of these', 'errorText' => implode(', ', $config->portal));
//}

// call method
if (in_array($getParams['method'], $config->methods[$acl]) && !$result['error']){
    require ($root.'methods.php');
	$params = [
			'method' => $getParams['method'],
			'api_key' => $getParams['get_params']['APIKey'],
			'request' => json_encode(getPostParams()),
			'active' => 1
		];
	addLog ($params);
    $result = call_user_func_array($getParams['method'], array($getParams['url_params']));
}

// error - Unknown error
if (count($result) == 0){
    $result = array('error' => true, 'errorCodeNumber' => 100, 'errorCode' => 'Unknown error (nothing to return)');
}

$json_result = json_encode($result, JSON_PRETTY_PRINT);


//TODO: encrypt response to APEX
$json_result = encrypySimpleAES($json_result);


header('Content-type: application/json');
echo $json_result;
exit();


?>