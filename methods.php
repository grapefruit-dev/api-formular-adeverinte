<?php


use Appwrite\ClamAV\Network;

require_once 'vendor/autoload.php';


function getCertificatesList($params)
{
    global $DB;
    $ret = array();
    $DB->connectDB();

    $post_params = getPostParams();

    if ($post_params['certificates'] == 0) {
        $items = getList("certificate_requests", "id", "WHERE `active` = 1 AND `downloaded` = 0", "*", "", false);
        foreach ($items as $k => $v) {
            unset($v['email']);
            unset($v['file_1']);
            unset($v['file_2']);
            unset($v['ip']);
            unset($v['useractivate']);
            unset($v['updated']);


            $user_data = json_decode(decrypt($v['user_data']));
            foreach ($user_data as $k1 => $v1) {
                $v[$k1] = $v1;
            }
            if (isset($v['tip-adeverinta']) && is_array($v['tip-adeverinta'])) {
                $i = 0;
                foreach ($v['tip-adeverinta'] as $k2 => $v2) {
                    $i++;
                    $v['tip-adeverinta' . $i] = $v2;
                }
                //unset($item['tip-adeverinta']);
            }
            //$v['user_data'] = decrypt($v['user_data']);
            unset($v['user_data']);
            $ret[] = $v;
        }
    } else if ($post_params['certificates'] == 1) {
        $item = getList("certificate_requests", "id", "WHERE active = 1 AND `id` = '" . $post_params['id'] . "' AND `hash` = '" . $post_params['hash'] . "' ", "*", "", true);
        unset($item['email']);
        unset($item['file_1']);
        unset($item['file_2']);
        unset($item['ip']);
        unset($item['useractivate']);
        unset($item['updated']);


        $user_data = json_decode(decrypt($item['user_data']));
        foreach ($user_data as $k1 => $v1) {
            $item[$k1] = $v1;
        }
        if (isset($item['tip-adeverinta']) && is_array($item['tip-adeverinta'])) {
            $i = 0;
            foreach ($item['tip-adeverinta'] as $k2 => $v2) {
                $i++;
                $item['tip-adeverinta' . $i] = $v2;
            }
            unset($item['tip-adeverinta']);
        }
        //$item['user_data'] = decrypt($item['user_data']);
        unset($item['user_data']);
        $ret = $item;

    } else {
        $ret = array('error' => true, 'errorCodeNumber' => 110, 'errorCode' => 'Wrong parameters');
    }


    $DB->close_db();
    return $ret;
}

function confirmCertificatesList($params)
{
    global $DB;
    $ret = array();
    $DB->connectDB();

    $post_params = getPostParams();

    $ret['updated'] = 0;
    foreach ($post_params as $k => $v) {
        $DB->query("UPDATE `certificate_requests` SET `downloaded` = '1' WHERE `id` = '" . $k . "' AND `hash` = '" . $v . "'");
        $ret['updated'] = $ret['updated'] + 1;
    }

    $DB->close_db();
    return $ret;
}

function deleteInactiveData()
{
    global $DB;
    $DB->connectDB();
    $DB->query("DELETE FROM `certificate_requests` WHERE `active` = '0' AND `created` <= (now() - interval 72 hour)");
    $DB->close_db();
    exit();
}

function deleteOldData()
{
    global $DB;
    $DB->connectDB();
    $DB->query("DELETE FROM `certificate_requests` WHERE `downloaded` = '1' AND `active` = '1' AND `status` IN (2, 3) AND `updated` <= (now() - interval 1 month)");
    $DB->close_db();
    exit();
}

function deleteCertificatesList($params)
{
    global $DB;
    $ret = array();
    $DB->connectDB();

    $post_params = getPostParams();

    $ret['processed'] = 0;
    foreach ($post_params as $k => $v) {
        $DB->query("DELETE FROM `certificate_requests` WHERE `downloaded` = '1' AND `active` = '1' AND `status` IN (2, 3) AND `id` = '" . $k . "' AND `hash` = '" . $v . "'");
        $ret['processed'] = $ret['processed'] + 1;
    }

    $DB->close_db();
    return $ret;
}

function getFile($params)
{
    global $DB;
    $ret = array();
    $DB->connectDB();

    $post_params = getPostParams();
    $items = getList("certificate_requests", "id", "WHERE active = 1 AND `id` = '" . $post_params['id'] . "' AND `hash` = '" . $post_params['hash'] . "'", "*", "", true);

    if ($post_params['file'] == 1) {
        echo encrypySimpleAES(base64_encode(base64_encode(decrypt(base64_decode($items['file_1'])))));
        //echo encrypySimpleAES(base64_encode($items['file_1']));
    } else if ($post_params['file'] == 2) {
        echo encrypySimpleAES(base64_encode(base64_encode(decrypt(base64_decode($items['file_2'])))));
        //echo encrypySimpleAES(base64_encode($items['file_2']));
    }

    $DB->close_db();
    exit();
}

function setCertificateStatus($params)
{
    global $DB, $smarty;
    $ret = array();
    $DB->connectDB();

    $post_params = getPostParams();
    $ret['error'] = true;
    if (isset($post_params['status']) && in_array($post_params['status'], array(2, 3))) {

        $DB->query("UPDATE `certificate_requests` SET `status` = '" . $post_params['status'] . "' WHERE active = 1 AND `id` = '" . $post_params['id'] . "' AND `hash` = '" . $post_params['hash'] . "' ");
        $ret['status'] = 'status set';
        if ($post_params['send_email'] == 1) {
            $item = getList("certificate_requests", "id", "WHERE active = 1 AND `id` = '" . $post_params['id'] . "' AND `hash` = '" . $post_params['hash'] . "' ", "*", "", true);
            $email = $post_params['email'] ? $post_params['email'] : decrypt($item['email']);
            $item['user_data_decrypt'] = json_decode(decrypt($item['user_data']));
            $smarty->assign("certificate", $item);

            if ($post_params['status'] == 2) {
                $msg = $smarty->fetch("finished.tpl");
            } else if ($post_params['status'] == 3) {
                $smarty->assign("message", $post_params['message']);
                $msg = $smarty->fetch("canceled.tpl");
            }

            if ($_SERVER['HTTP_HOST'] === "adeverinte-dacia.ro") {
                send_mail_sendgrid($email, 'info@adeverinte-dacia.ro', "Informare status cerere", $msg);
            } else {
                send_mail_sendgrid($email, 'info@adeverinte-dacia.gd.ro', "Informare status cerere - dev", $msg);
            }
            $ret['email'] = 'email sent';
        }
        $ret['error'] = false;
    }


    $DB->close_db();
    return $ret;
}


function getTimestamp($params)
{
    global $DB;
    $ret = array();
    $DB->connectDB();

    $ret = $DB->getmicrotime();
    $ret = explode(".", $ret);
    $ret = date("Y-d-m H:i:s", $ret[0]) . "." . $ret[1];
    $ret = base64_encode($ret);

    $DB->close_db();
    return $ret;
}


function addCertificate(){
    global $DB, $smarty;
    $data = array();
    $error = array();
    $fileMaxSize = "2097152";
    $secondFileMaxSize = "10485760";
    $DB->connectDB();
    $allowedExt = array('jpg', 'jpeg', 'png', 'zip', 'rar', 'pdf');

    $post_params = getPostParams();
    $adevetinte = [
        '1' => 'sporurile cu caracter permanent care fac parte din baza de calcul a pensiei',
        '2' => 'grupă muncă',
        '3' => 'orele lucrate în timpul nopții',
        '4' => 'salariul lunar brut',
    ];

    if (count($post_params) > 0){
        foreach ($post_params as $k => $v) {
            if (preg_match("/[a-z\-\_]/i", $k)) {
                if (isJson($v)){
                    $v = json_decode($v, true);
                }
                if (!is_array($v)){
                    $k = str_replace("-", "_", $k);
                    $data[$k] = $$k = addslashes(trim(strip_tags($v)));
                } else {
                    $k = str_replace("-", "_", $k);
                    //$i=0;
                    foreach ($v as $k1 => $v1) {
                        //$i++;
                        $name = $k."".intval($v1);
                        $data[$name] = $$name = $adevetinte[intval($v1)];
                    }
                }
            }
        }
    }

    $error = validateInput(
        $cname,
        $email,
        $cname_father,
        $cname_mother,
        $cnp,
        $ctel,
        $angajat,
        $bday,
        $bplace,
        $bplace_city,
        $judet,
        $domiciliu_city,
        $cstreet,
        $nrstreet,
        $societate,
        $startdate,
        $enddate,
        $scop_adeverinta,
        $fileMaxSize,
        $secondFileMaxSize,
        $allowedExt,
        $tip_adeverinta1,
        $tip_adeverinta2,
        $tip_adeverinta3,
        $tip_adeverinta4
    );

    if (count($error) == 0) {

        $user_data = encrypt(json_encode($data));
        $email_data = encrypt("" . $email);

        $file1 = base64_encode(encrypt(file_get_contents($_FILES['file1']['tmp_name'])));
        $file2 = base64_encode(encrypt(file_get_contents($_FILES['file2']['tmp_name'])));

        $file1ext = pathinfo($_FILES['file1']['name'], PATHINFO_EXTENSION);
        $file2ext = pathinfo($_FILES['file2']['name'], PATHINFO_EXTENSION);

        $hash = md5(uniqid(time() . mt_rand(1, 10000000)));
        $useractivate = md5(uniqid(time() . mt_rand(1, 10000000)));


        $DB->insertSanitized(
            NULL,
            '',
            $hash,
            $user_data,
            $email_data,
            $file1,
            $file2,
            $file1ext,
            $file2ext,
            '0',
            '0',
            $useractivate,
            date("Y-m-d H:i:s"),
            date("Y-m-d H:i:s"),
            '0'
        );

        $id = $DB->get_insert_id();

        if ($id > 0) {
            $certificate = getList("certificate_requests", "id", "WHERE active=0 AND id>'0' AND id='" . $id . "' LIMIT 1", "*", "", true);

            $certificate['user_data_decrypt'] = json_decode(decrypt($certificate['user_data']));
            $certificate['email_decrypt'] = decrypt($certificate['email']);

            $email = $certificate['email_decrypt'];

            $smarty->assign("certificate", $certificate);
            $msg = $smarty->fetch("activate-email.tpl");

            if ($_SERVER['HTTP_HOST'] === "adeverinte-dacia.ro") {
                send_mail_sendgrid($email, 'info@adeverinte-dacia.ro', "Confirmare cerere eliberare adeverinta", $msg);
            } else {
                send_mail_sendgrid($email, 'info@adeverinte-dacia.gd.ro', "Confirmare cerere eliberare adeverinta - dev", $msg);
            }

            $DB->close_db();
            return array('error' => false, 'redirectedUrl' => '/juridic/solicitare-adeverinte-pensionare/confirmare-solicitare-adeverinta');
        } else {
            $DB->close_db();
            return array('error' => true, 'errorCodeNumber' => 100, 'errorCode' => 'Unknown error');
        }
    }

    $DB->close_db();
    return array('error' => true, 'errorCodeNumber' => 2, 'errorCode' => 'Missing required parameter', 'errorDetails' => $error);
}

function validateInput(
    $cname,
    $email,
    $cname_father,
    $cname_mother,
    $cnp,
    $ctel,
    $angajat,
    $bday,
    $bplace,
    $bplace_city,
    $judet,
    $domiciliu_city,
    $cstreet,
    $nrstreet,
    $societate,
    $startdate,
    $enddate,
    $scop_adeverinta,
    $fileMaxSize,
    $secondFileMaxSize,
    $allowedExt,
    $tip_adeverinta1,
    $tip_adeverinta2,
    $tip_adeverinta3,
    $tip_adeverinta4
)
{
    global $DB;
    $error = array();
    $rexSafety = '/[^a-zA-Z0-9_\- \p{L}]/u';

    if ($cname == '') {
        $error['cname'] = "Completați numele!";
    }
    $filteredCname = preg_replace($rexSafety, '', $cname);

    $filteredCname = htmlspecialchars($filteredCname, ENT_QUOTES, 'UTF-8');
    if ($cname !== $filteredCname || preg_match($rexSafety, $cname)) {
        $error['cname'] = "Numele nu poate conține caractere interzise!";
    }
    if (strlen($cname) > 50) {
        $error['cname'] = "Numele este prea lung!";
    }
    if (!isEmail($email)) {
        $error['email'] = "Emailul nu este valid!";
    }
    if ($cname_father == '') {
        $error['cname_father'] = "Completați prenumele tatălui!";
    }
    if (strlen($cname_father) > 30) {
        $error['cname_father'] = "Prenumele tatălui este prea lung!";
    }
    $filteredCnameFather = preg_replace($rexSafety, '', $cname_father);
    $filteredCnameFather = htmlspecialchars($filteredCnameFather, ENT_QUOTES, 'UTF-8');
    if (preg_match($rexSafety, $cname_father) || $cname_father !== $filteredCnameFather) {
        $error['cname_father'] = "Prenumele tatălui nu poate conține caractere interzise!";
    }
    if ($cname_mother == '') {
        $error['cname_mother'] = "Completați prenumele mamei!";
    }
    $filteredCnameMother = preg_replace($rexSafety, '', $cname_mother);
    $filteredCnameMother = htmlspecialchars($filteredCnameMother, ENT_QUOTES, 'UTF-8');
    if (preg_match($rexSafety, $cname_mother) || $cname_mother !== $filteredCnameMother) {
        $error['cname_mother'] = "Prenumele mamei nu poate conține caractere interzise!";
    }
    if (strlen($cname_mother) > 30) {
        $error['cname_mother'] = "Prenumele mamei este prea lung!";
    }
    if (!isMobilePhone($ctel)) {
        $error['ctel'] = "Completați numărul de telefon!";
    }
    if ($angajat == '') {
        $error['angajat'] = "Alegeți tipul de angajat!";
    }
    if ($bday == '') {
        $error['bday'] = "Completați data nașterii!";
    }
    if (!preg_match('/^\d{4}-\d{2}-\d{2}$/', $bday)) {
        $error['bday'] = "Data nașterii nu respectă formatul AAAA-LL-ZZ!";
    }

    if ($bplace == '') {
        $error['bplace'] = "Completați locul nașterii!";
    }
    $filteredBPlace = preg_replace($rexSafety, '', $bplace);
    $filteredBPlace = htmlspecialchars($filteredBPlace, ENT_QUOTES, 'UTF-8');
    if (preg_match($rexSafety, $bplace) || $bplace !== $filteredBPlace) {
        $error['bplace'] = "Locul nașterii nu poate conține caractere interzise!";
    }
    if (strlen($bplace) > 30) {
        $error['bplace'] = "Locul nașterii este prea lung!";
    }
    if ($bplace_city == '') {
        $error['bplace_city'] = "Completați locul nașterii!";
    }
    $filteredBPlaceCity = preg_replace($rexSafety, '', $bplace_city);
    $filteredBPlaceCity = htmlspecialchars($filteredBPlaceCity, ENT_QUOTES, 'UTF-8');
    if (preg_match($rexSafety, $bplace_city) || $bplace_city !== $filteredBPlaceCity) {
        $error['bplace_city'] = "Locul nașterii nu poate conține caractere interzise!";
    }
    if (strlen($bplace_city) > 30) {
        $error['bplace_city'] = "Locul nașterii este prea lung!";
    }
    if ($judet == '') {
        $error['judet'] = "Completați județul de domiciliu!";
    }
    if ($domiciliu_city == '') {
        $error['domiciliu_city'] = "Completați orașul de domiciliu!";
    }
    $filteredDomiciliuCity = preg_replace($rexSafety, '', $domiciliu_city);
    $filteredDomiciliuCity = htmlspecialchars($filteredDomiciliuCity, ENT_QUOTES, 'UTF-8');
    if (preg_match($rexSafety, $domiciliu_city) || $domiciliu_city !== $filteredDomiciliuCity) {
        $error['domiciliu_city'] = "Orașul de domiciliu nu poate conține caractere interzise!";
    }
    if (strlen($domiciliu_city) > 30) {
        $error['domiciliu_city'] = "Orașul de domiciliu este prea lung!";
    }
    if ($cstreet == '') {
        $error['cstreet'] = "Completați strada!";
    }
    if (strlen($cstreet) > 50) {
        $error['cstreet'] = "Strada are prea multe caractere!";
    }
    $filteredCStreet = preg_replace($rexSafety, '', $cstreet);
    $filteredCStreet = htmlspecialchars($filteredCStreet, ENT_QUOTES, 'UTF-8');
    if (preg_match($rexSafety, $cstreet) || $cstreet !== $filteredCStreet) {
        $error['cstreet'] = "Strada nu poate conține caractere interzise!";
    }
    if ($nrstreet == '') {
        $error['nrstreet'] = "Completați numărul străzii!";
    }
    $filteredNrStreet = preg_replace($rexSafety, '', $nrstreet);
    $filteredNrStreet = htmlspecialchars($filteredNrStreet, ENT_QUOTES, 'UTF-8');
    if (preg_match($rexSafety, $nrstreet) || $nrstreet !== $filteredNrStreet) {
        $error['nrstreet'] = "Numărul străzii nu poate conține caractere interzise!";
    }
    if (strlen($cstreet) > 50) {
        $error['nrstreet'] = "Numărul străzii este prea lung!";
    }
    if (!validCNP($cnp)){
        $error['cnp'] = "CNP nu este valid!";
    }
    if ($societate == '') {
        $error['societate'] = "Completați societatea angajatoare!";
    }
    $filteredSociety = preg_replace($rexSafety, '', $societate);
    $filteredSociety = htmlspecialchars($filteredSociety, ENT_QUOTES, 'UTF-8');
    if (preg_match($rexSafety, $societate) || $societate !== $filteredSociety) {
        $error['societate'] = "Societatea angajatoare nu poate conține caractere interzise!";
    }
    if (strlen($cstreet) > 50) {
        $error['societate'] = "Societatea angajatoare are prea multe caractere!";
    }
    if ($startdate == '') {
        $error['startdate'] = "Completați perioada lucrată!";
    }
    if (!preg_match('/^\d{4}-\d{2}-\d{2}$/', $startdate)) {
        $error['startdate'] = "Data de început a perioadei lucrate nu respectă formatul AAAA-LL-ZZ!";
    }
    if ($enddate == '') {
        $error['enddate'] = "Completați perioada lucrată!";
    }
    if (!preg_match('/^\d{4}-\d{2}-\d{2}$/', $enddate)) {
        $error['enddate'] = "Data de sfârșit a perioadei lucrate nu respectă formatul AAAA-LL-ZZ!";
    }
    if ($scop_adeverinta == '') {
        $error['scop_adeverinta'] = "Selectați motivul eliberării adeverinței!";
    }
    if (!isset($tip_adeverinta1) && !isset($tip_adeverinta2) && !isset($tip_adeverinta3) && !isset($tip_adeverinta4)){
        $error['tip_adeverinta'] = "Selectați tipul adeverinței solicitate!";
    }
    if (!isset($_FILES['file1']['tmp_name'])) {
        $error['file1'] = "Atașați copie carte de identitate!";
    }
    if ($_FILES['file1']['size'] > $fileMaxSize) {
        $error['file1'] = "Copia cărții de identitate are o dimensiune prea mare!";
    }
    $filename1 = explode(".", $_FILES['file1']['name']);
    if (!in_array(end($filename1), $allowedExt)) {
        $error['file1'] = "Fișierul corespunzător copiei cărții de identitate nu este permis!";
    }

    //ClamAV
//        $clam = new Network('143.244.207.58', 3310);
        if (isset($_FILES['file1']['tmp_name'])) {
            $fileTmpPath = $_FILES['file1']['tmp_name'];
//            $result = $clam->fileScan($fileTmpPath);
//            if (!$result) {
//                $error['file1'] = "Fișierul corespunzător copiei cărții de identitate nu este sigur!";
//            }
        }

        if (!isset($_FILES['file2']['tmp_name'])) {
            $error['file2'] = "Atașați copie carte de muncă!";
        }
        if ($_FILES['file2']['size'] > $secondFileMaxSize) {
            $error['file2'] = "Copia cărții de muncă are o dimensiune prea mare!";
        }
        $filename2 = explode(".", $_FILES['file2']['name']);
        if (!in_array(end($filename2), $allowedExt)) {
            $error['file2'] = "Fișierul corespunzător copiei cărții de muncă nu este permis!";
        }
        //ClamAV
        if (isset($_FILES['file2']['tmp_name'])) {
            $fileTmpPath = $_FILES['file2']['tmp_name'];
//            $result = $clam->fileScan($fileTmpPath);
//            if (!$result) {
//                $error['file2'] = "Fișierul corespunzător copiei cărții de muncă nu este sigur!";
//            }
        }

        return $error;
    }

?>
