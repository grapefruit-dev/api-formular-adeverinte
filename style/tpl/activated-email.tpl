<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta name="format-detection" content="telephone=no">
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=no;">
<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
{literal}
    <style type="text/css">

        /* Some resets and issue fixes */
        #outlook a { padding:0; }
        body{ width:100% !important; -webkit-text; size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0; }
        .ReadMsgBody { width: 100%; }
        .ExternalClass {width:100%;}
        .backgroundTable {margin:0 auto; padding:0; width:100%;!important;}
        table td {border-collapse: collapse;}
        .ExternalClass * {line-height: 115%;}
        /* End reset */

        .hide { max-height: 0px; font-size: 0; display: none; }


        /* These are our tablet/medium screen media queries */
        @media screen and (max-width: 600px){


            /* Display block allows us to stack elements */
            *[class="mobile-column"] {display: block;}

            /* Some more stacking elements */
            *[class="mob-column"] {float: none !important;width: 100% !important;}

            /* Hide stuff */
            *[class="hide"] {display:none !important;}

            /* This sets elements to 100% width and fixes the height issues too, a god send */
            *[class="100p"] {width:100% !important; height:auto !important;}

            /* For the 2x2 stack */
            *[class="condensed"] {padding-bottom:40px !important; display: block;}

            /* Centers content on mobile */
            *[class="center"] {text-align:center !important; width:100% !important; height:auto !important;}

            /* 100percent width section with 20px padding */
            *[class="100pad"] {width:100% !important; padding:20px;}

            /* 100percent width section with 20px padding left & right */
            *[class="100padleftright"] {width:100% !important; padding:0 20px 0 20px;}

            /* 100percent width section with 20px padding top & bottom */
            *[class="100padtopbottom"] {width:100% !important; padding:20px 0px 20px 0px;}

            .hide { max-height: none !important; font-size: 12px !important; display: block !important; }
            .hide-on-mobile { max-height: 0px; font-size: 0; display: none; }

        }
    </style>
{/literal}
</head>
<body style="padding:0; margin:0">

<table border="0" cellpadding="0" cellspacing="0" style="margin: 0 auto; padding: 0; max-width: 600px;"  width="100%">
    <tr>
        <td height="20"></td>
    </tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" style="margin: 0 auto; padding: 0; max-width: 600px; border: 1px solid #BBBCBC; border-radius: 4px;" width="100%" class="">
    <tr>
        <td align="center" valign="top">

            <table width="100%" border="0" cellspacing="0" cellpadding="20" class="100p">
                <tr>
                    <td align="left" style="font-size:18px; font-weight: bold; line-height: 22px; text-transform: uppercase; color:#443c4e;">
                        <img src="https://{$smarty.server.HTTP_HOST}/api/style/images/emails/logo-dacia.png" alt="Dacia" style="display: block; border: 0;" border="0">
                     </td>
                </tr>
            </table>

            <table width="100%" cellspacing="0" cellpadding="0" style="background-color: #4cc2cc;" class="hide-on-mobile">
                <tr>
                    <td bgcolor="#ffffff" width="100%" valign="top" class="hide-on-mobile" style="max-width: 600px;">
                        <img src="https://{$smarty.server.HTTP_HOST}/api/style/images/emails/header.jpg" alt="" style="width: 100%; display: block;" border="0" />
                    </td>
                </tr>
            </table>

            <table width="100%" border="0" cellspacing="0" cellpadding="20" class="100p">
                <tr>
                    <td align="center">

                        <table width="100%" border="0" cellspacing="0" cellpadding="0"  class="100p">
                            <tr>
                                <td height="20"></td>
                            </tr>
                            <tr>
                                <td align="left" style="font-family: Arial, Helvetica, sans-serif; font-size:16px; line-height: 18px; color:#040404;">
                                    Bună ziua,<br/><br />
                                    Acest email este o confirmare a înregistrării solicitării dumneavoastră pe site-ul <a href="http://dacia.ro/">dacia.ro</a>.
                                </td>
                            </tr>

                            <tr>
                                <td height="40"></td>
                            </tr>

                            <tr>
                                <td align="center" style="font-size:10px; line-height: 12px; color:#443c4d;">
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" >
                                        <thead bgcolor="#f1f1f2" style="background: #F1F1F2;">
                                            <tr>
                                                <td style="color: #040404; font-family: Arial, Helvetica, sans-serif; font-weight: bold; font-size: 15px; line-height: 18px; padding: 10px">
                                                    Detalii solicitare
                                                </td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; line-height: 18px; padding: 5px 10px;">
                                                    Număr solicitare: {$certificate.register_id}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; line-height: 18px; padding: 5px 10px;">
                                                    Data creare solicitare: {$certificate.created|date_format:"%d/%m/%Y"}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; line-height: 18px; padding: 5px 10px;">
                                                    Adeverințe solicitate:
                                                    {if $certificate.user_data_decrypt->tip_adeverinta1 != ''}
                                                    	{$certificate.user_data_decrypt->tip_adeverinta1};
                                                    {/if}
                                                    {if $certificate.user_data_decrypt->tip_adeverinta2 != ''}
                                                    	{$certificate.user_data_decrypt->tip_adeverinta2};
                                                    {/if}
                                                    {if $certificate.user_data_decrypt->tip_adeverinta3 != ''}
                                                    	{$certificate.user_data_decrypt->tip_adeverinta3};
                                                    {/if}
                                                    {if $certificate.user_data_decrypt->tip_adeverinta4 != ''}
                                                    	{$certificate.user_data_decrypt->tip_adeverinta4};
                                                    {/if}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; line-height: 18px; padding: 5px 10px;">
                                                    <strong>Status: solicitare înregistrată</strong>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>

                            <tr>
                                <td height="30"></td>
                            </tr>

                            <tr>
                                <td align="left" style="font-family: Arial, Helvetica, sans-serif; font-size:16px; line-height: 18px; color:#040404;">
                                    Mai multe informații cu privire la stadiul de avansare a solicitării veți primi în perioada următoare pe email.<br /><br />
                                    Vă mulțumim pentru interesul acordat.
                                </td>
                            </tr>

                            <tr>
                                <td height="30"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>

            <table width="100%" border="0" cellspacing="0" cellpadding="20" class="100p">
                <tr>
                    <td align="left" style="font-family: Arial, Helvetica, sans-serif; font-size:15px; line-height: 18px; color:#040404;">
                        O zi bună,<br />
                        Echipa Dacia România
                    </td>
                </tr>
                <tr>
                    <td height="30"></td>
                </tr>
            </table>

            <table width="100%" border="0" cellspacing="0" cellpadding="20" class="100p">
                <tr>
                    <td align="left" style="font-family: Arial, Helvetica, sans-serif; font-size:15px; line-height: 18px; color:#999999;">
                        Acest email și orice atașamente reprezintă o corespondență confidențială destinată numai utilizării persoanei fizice care a completat/în numele căreia a completat formularul electronic. Dacă nu sunteți destinatarul intenționat sau persoana fizică responsabilă pentru transmiterea mesajului destinatarului intenționat, sunteți înștiințat că orice divulgare, distribuire sau copiere a acestei comunicări este strict interzisă. Dacă ați primit această comunicare din greșeală, vă rugăm să anunțați expeditorul răspunzând la acest mesaj, apoi ștergeți corespondența cu noi din sistemul dvs.
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>
